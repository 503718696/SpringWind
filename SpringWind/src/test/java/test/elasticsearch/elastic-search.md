

# ElasticSearch 分布式搜索引擎


> 下载 ZIP 压缩包

```
https://www.elastic.co/downloads/elasticsearch
```

> 安装使用

```
1、运行：sudo bin/elasticsearch 

2、访问：http://localhost:9200/，可以看到返回一个类似于JSON式的结果集。
```

> 安装 Elasticsearch的可视化管理和监控工具，Marvel（和漫威同名了）：

```
1、重新启动Elasticsearch：
sudo bin/plugin -i elasticsearch/marvel/latest
sudo bin/elasticsearch restart

2、安装 Marvel可以访问这里：http://localhost:9200/_plugin/marvel
```

> 相关文档

```
1、Java API的文档
https://www.elastic.co/guide/en/elasticsearch/client/java-api/current/index.html

2、ElasticSearch权威指南（中文版）
http://es.xiaoleilu.com/010_Intro/00_README.html
```



