package com.GreenBamboo.springwind.codeSystem;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.junit.Test;

/**
 * 测试通过,详细见测试报告：
 * SpringWind\SpringWind\src\test\java\com\GreenBamboo\springwind\codeSystem\testReport\ACodeerTest_pass_report.docx
 *
 * @author bing <503718696@qq.com>
 */
public class ACodeerTest {

    public ACodeerTest() {
    }

    @Test
    public void testGetCaptcha() {
        try {
            System.out.println("getCaptcha");
            ACodeer instance = new ACodeer();
            for (int i = 0; i < 8; i++) {
                ImageCode result = instance.getCaptcha();
                ImageIO.write((RenderedImage) result.getImage(), "png", new File("D:\\codeerTest\\" + result.getCode() + ".png"));
            }

        } catch (IOException ex) {
            Logger.getLogger(ACodeerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
