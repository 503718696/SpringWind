package com.GreenBamboo.springwind.codeSystem;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.junit.Test;

/**
 *
 * @author bing <503718696@qq.com>
 */
public class CCodeerTest {

	public CCodeerTest() {
	}

	/**
	 * Test of getCaptcha method, of class CCodeer.
	 */
	@Test
	public void testGetCaptcha() {
		try {
			System.out.println("GetCaptcha");
			CCodeer instance = new CCodeer();
			for (int i = 0; i < 10; i++) {
				ImageCode result = instance.getCaptcha();

				System.out.println("Image= " + result.getImage() + " code= " + result.getCode());

				ImageIO.write((RenderedImage) result.getImage(), "png",
						new File("D:\\codeerTest\\" + result.getCode() + ".png"));
			}

		} catch (IOException ex) {
			Logger.getLogger(CCodeerTest.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
